{
  outputs = { self, nixpkgs }:
  let pkgs = nixpkgs.legacyPackages.x86_64-linux; in
  rec {

    packages.x86_64-linux.viz = pkgs.stdenv.mkDerivation {
      pname = "viz";
      version = "1.0";

      src = self;

      nativeBuildInputs = with pkgs; [ zig ];

      dontConfigure = true;

      preBuild = ''
        export HOME=$TMPDIR
      '';

      buildPhase = ''
        runHook preBuild

        zig build -Drelease-fast -Dcpu=baseline

        runHook postBuild
      '';

      installPhase = ''
        runHook preInstall

        zig build -Drelease-fast -Dcpu=baseline --prefix $out install

        runHook postInstall
      '';
    };

    defaultPackage.x86_64-linux = packages.x86_64-linux.viz;
  };
}
