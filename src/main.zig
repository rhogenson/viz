const std = @import("std");
const c = @cImport({
    @cInclude("sys/ioctl.h");
});

var win_sz: struct {
    lines: u32 = 0,
    cols: u32 = 0,

    const Self = @This();

    fn resize(self: *Self) !bool {
        var w: c.winsize = undefined;
        if (std.os.linux.ioctl(0, c.TIOCGWINSZ, @ptrToInt(&w)) < 0) {
            return error.IOCtlInternal;
        }
        const changed = w.ws_row != self.lines or w.ws_col != self.cols;
        self.lines = w.ws_row;
        self.cols = w.ws_col;
        return changed;
    }
} = undefined;

fn idx2(buf: []u8, i: usize, j: usize) *u8 {
    return &buf[i * win_sz.cols + j + 1];
}

fn alloc_buf(allocator: std.mem.Allocator) ![]u8 {
    const buf_sz = win_sz.lines * win_sz.cols + 1;
    var buf = try allocator.alloc(u8, buf_sz);
    buf[0] = '\n';
    return buf;
}

fn angle(i: i64, j: i64) f64 {
    const y = @intToFloat(f64, (win_sz.lines / 2 - i) * 3);
    const x = @intToFloat(f64, j - win_sz.cols / 2);
    var atan = std.math.atan2(f64, y, x);
    if (atan < 0) {
        atan += 2 * std.math.pi;
    }
    return atan + @sqrt(x * x + y * y) / 100;
}

fn sun(t: i64, buf: []u8) void {
    const offset = @intToFloat(f64, t) * std.math.pi * 2 / 10 / 30;
    var i: usize = 0;
    while (i < win_sz.lines) : (i += 1) {
        var j: usize = 0;
        while (j < win_sz.cols) : (j += 1) {
            const a = angle(@intCast(i64, i), @intCast(i64, j)) + offset;
            const branches = 7;
            const ai = @floatToInt(i64, a * 16 * branches / std.math.pi);
            switch (@rem(ai, 32)) {
                0, 20 => {
                    idx2(buf, i, j).* = '`';
                },
                1, 19 => {
                    idx2(buf, i, j).* = '\'';
                },
                2, 18 => {
                    idx2(buf, i, j).* = '-';
                },
                3, 17 => {
                    idx2(buf, i, j).* = '~';
                },
                4, 16 => {
                    idx2(buf, i, j).* = '+';
                },
                5, 15 => {
                    idx2(buf, i, j).* = '*';
                },
                6...14 => {
                    idx2(buf, i, j).* = '#';
                },
                else => {
                    idx2(buf, i, j).* = ' ';
                },
            }
        }
    }
}

pub fn main() !void {
    var gp = std.heap.GeneralPurposeAllocator(.{}){};
    var allocator = gp.allocator();
    const stdout = std.io.getStdOut().writer();

    _ = try win_sz.resize();
    var buf: []u8 = try alloc_buf(allocator);

    var timer = try std.time.Timer.start();
    var last_update: u64 = 0;
    while (true) {
        var now = timer.read();
        if (now - last_update > std.time.ns_per_s) {
            last_update = now;
            if (try win_sz.resize()) {
                allocator.free(buf);
                buf = try alloc_buf(allocator);
            }
        }

        const frame = std.time.ns_per_s / 30;
        const t = @intCast(i64, now / frame);
        sun(t, buf);
        _ = try stdout.write(buf);
        std.time.sleep(std.time.ns_per_ms);
    }
}
